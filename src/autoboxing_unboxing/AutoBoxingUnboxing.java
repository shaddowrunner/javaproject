package autoboxing_unboxing;

/**
 * Created by Shaddowrunner on 17.06.2017.
 */
public class AutoBoxingUnboxing {
    public static void main(String[] args) {
        int a = 10;
        Integer i = new Integer(5);

        a = i;//unboxing

        i = a;//new Integer(a) - autoboxing
    }
}
