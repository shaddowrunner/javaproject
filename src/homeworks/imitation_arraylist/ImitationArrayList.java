package homeworks.imitation_arraylist;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.time.LocalDateTime;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;


public class ImitationArrayList {
    public static final String PATH_TO_FILE = "./textfiles/logs.txt";
    public static final Path PATH = Paths.get(PATH_TO_FILE);


    public String[] split(String regex) {
        return new String[10];
    }

    private int[] array;//0

    public ImitationArrayList() {
        array = new int[10];//0 -> 5, 6, 4, 9, 0, 0
    }


    //    1) добавление элементов.

    public void addElement(int value) {
        reSize();
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                array[i] = value;
                String log = LocalDateTime.now() + " addElement -> input parameters: " + value + ". ";
                writeLog(log);
                break;
            }
        }
    }

//    writeLog(String log)

    public void deleteElement(int index) {
        int[] temp = new int[array.length - 1];

        for (int i = 0; i < index; ++i) {
            temp[i] = array[i];
        }

        for (int i = index; i < array.length; ++i) {
            if (i == temp.length) {
                break;
            }
            temp[i] = array[i + 1];
        }

        array = temp;
        String log = LocalDateTime.now() + " deleteElement -> output parameters: " + array[index] + ". ";
        writeLog(log);

    }


    private void reSize() {
        boolean shouldReSize = false;

        for (int element : array) {
            if (element == 0) {
                shouldReSize = true;
                break;
            }
        }

        if (!shouldReSize) {
            int[] temp = new int[array.length * 2];

            for (int i = 0; i < array.length; i++) {
                temp[i] = array[i];
            }
            array = temp;
            String log = LocalDateTime.now() + " reSize -> twice size parameter: " + array.length + ". ";
            writeLog(log);
        }

    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    //2) увеличение листа на заданное количество элементов.

    public void addSize(int newSize) {
        int[] temp = new int[array.length + newSize];
        for (int i = 0; i < array.length; i++) {
            temp[i] = array[i];
        }
        array = temp;
        String log = LocalDateTime.now() + " addSize -> input new size parameters: " + newSize + ". ";
        writeLog(log);
    }

    //3) уменьшение листа до заданного количество элементов.


    public void decreaseSize(int newSize) {
        int[] temp2 = new int[array.length - newSize];//10
        for (int i = 0; i < temp2.length; i++) {
            temp2[i] = array[i];
        }
        array = temp2;
        String log = LocalDateTime.now() + " decreaseSize -> output new size parameters: " + newSize + ". ";
        writeLog(log);
    }

//      2) изменение элемента по индексу.

    public void changeElementByIndex(int index, int value) {
        array[index] = value;
        String log =
                LocalDateTime.now() + " changeElementByIndex -> input parameter: " + value + " by index " + index + ". ";
        writeLog(log);
    }

    //    вывод элементов в консоль в прямом порядке.
    public void showElementsByRightOrder() {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }

    public void showElementsByInverseOrder() {
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.print(array[i] + " ");
        }
    }

    //Cортировка листа(пузырьковая сортировка).

    public void bubbleSort() {
        for (int i = 0; i < array.length ; i++) {
            for (int j = 0; j < array.length - 1; j++) {
                if (array[j] < array[j + 1]) {
                    int temp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }

            }
        }

    }


    public void writeLog(String log) {

        try (BufferedWriter writer = Files.newBufferedWriter(PATH, StandardOpenOption.APPEND)) {
            writer.write(log + "\n", 0, log.length());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showLogs() {
        System.out.println(readLog());
    }

    private String readLog() {

        StringBuilder builder = new StringBuilder();

        try (BufferedReader reader = Files.newBufferedReader(PATH)) {

            String line;

            while ((line = reader.readLine()) != null) {
                builder.append(line + "#");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return builder.toString();
    }

    public void showLogsByMethod(String searchParam) {
        String[] logs = readLog().split("#");
        for (String log : logs) {
            if (log.contains(searchParam)) {
                System.out.println(log);
            }
        }
    }

}
