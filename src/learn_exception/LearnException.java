package learn_exception;

import java.io.IOException;

/**
 * Created by Shaddowrunner on 17.06.2017.
 */
public class LearnException {

    public static void throwException() {
        try {
            throw new IOException();
        } catch (IOException e) {
            System.out.println("I'm exception");
        }

        catch (Exception e) {
            System.out.println("I'm exception");
        }
    }

    public static void throwException1() throws IOException {
        throw new IOException();

    }

    public static void throwException2()  {
        throw new NullPointerException();

    }

    public static void throwException3()  {
        try {
//            1
//            2
            throw new SlavaException("I' m Slava");//3
//            4
        } catch (SlavaException e) {
            System.out.println(e.getMessage());
        } finally {
            System.out.println("Finally");
        }

    }

    public static void main(String[] args) throws IOException {
        throwException3();
    }
}


class SlavaException extends IOException {
    public SlavaException(String message) {
        super(message);
    }
}