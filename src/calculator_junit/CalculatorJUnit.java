package calculator_junit;

import java.util.Random;

/**
 * Created by Shaddowrunner on 17.06.2017.
 */
public class CalculatorJUnit {
    Random random = new Random();

    public int plus() {
        int operandOne = getRandomNumberOne();
        int operandTwo = getRandomNumberTwo();
        return operandOne + operandTwo;
    }

    public int minus() {
        int operandOne = getRandomNumberOne();
        int operandTwo = getRandomNumberTwo();
        return operandOne - operandTwo;
    }

    public int multiply() {
        int operandOne = getRandomNumberOne();
        int operandTwo = getRandomNumberTwo();
        return operandOne * operandTwo;
    }

    public int degree() {
        int operandOne = getRandomNumberOne();
        int operandTwo = getRandomNumberTwo();
        return operandOne / operandTwo;
    }

    public int getRandomNumberOne() {
        return random.nextInt(20);
    }

    public int getRandomNumberTwo() {
        return random.nextInt(20);
    }


}
