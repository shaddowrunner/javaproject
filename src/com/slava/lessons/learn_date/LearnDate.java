package com.slava.lessons.learn_date;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by Shaddowrunner on 07.06.2017.
 */
public class LearnDate {
    public static void main(String[] args) {
        Date date = new Date(1465309979000l);

        /*System.out.println(date.getYear());
        System.out.println(date.getMonth());*/

//        System.out.println("Today's time -> " + new Date(1496846645249l));

        Calendar calendar = Calendar.getInstance();

//        GregorianCalendar

//        calendar.set(Calendar.YEAR, 2015);
//        calendar.add(Calendar.MONTH, -3);

        Date time = calendar.getTime();

//        System.out.println(calendar);

//        System.out.println("Changed time -> " + time);

        System.out.println(LocalDateTime.now());


    }
}
