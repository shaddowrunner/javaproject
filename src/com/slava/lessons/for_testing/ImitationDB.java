package com.slava.lessons.for_testing;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Shaddowrunner on 02.06.2017.
 */
public class ImitationDB {
    private List<String> data;

    public ImitationDB() {
        this.data = new ArrayList<>();
        data.addAll(Arrays.asList("Object1", "Object2", "Object3"));
    }

    public List<String> getAllRecords() {
        return data;
    }

    public void insertData(String object) {
        data.add(object);
    }

    public int getNumber() {
        return 12;
    }

}
