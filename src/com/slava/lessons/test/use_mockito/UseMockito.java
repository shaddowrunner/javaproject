package use_mockito;

import com.slava.lessons.for_testing.ImitationDB;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

/**
 * Created by Shaddowrunner on 02.06.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class UseMockito {

/*    @Mock
    ImitationDB mockDB = Mockito.mock(ImitationDB.class);*/

    @Mock
    ImitationDB mockDB;

    @Spy
    ImitationDB spyDB;


   /* @Test
    public void tryMockito() {
        ImitationDB imitationDB = new ImitationDB();

//        Mockito.when(mockDB.getAllRecords()).thenReturn(Arrays.asList("Hello"));

//        Assert.assertEquals("Should return \" Hello\"", "Hello", mockDB.getAllRecords().get(0));
        Assert.assertArrayEquals("Arrays should be equals ",
                Arrays.asList("Object1", "Object2", "Object3").toArray(), imitationDB.getAllRecords().toArray());
    }*/

    @Test
    public void trySpy() {
//        Mockito.when(spyDB.getNumber()).thenReturn(45);
        Mockito.doReturn(45).when(spyDB).getNumber();
        Assert.assertEquals(45, spyDB.getNumber());
    }

    @Test
    public void tryMockitoWithVerify() {

        Mockito.when(mockDB.getAllRecords()).thenReturn(Arrays.asList("Hello"));

        mockDB.getAllRecords();

        Assert.assertEquals("Should return \" Hello\"", "Hello", mockDB.getAllRecords().get(0));

//        Mockito.verify(mockDB, Mockito.times(1)).getAllRecords();

//        Mockito.verify(mockDB, Mockito.atLeast(1)).getAllRecords();

//        Mockito.verify(mockDB, Mockito.atMost(3)).getAllRecords();
        //Mockito.verify(mockDB, Mockito.never()).getAllRecords();
    }
}
