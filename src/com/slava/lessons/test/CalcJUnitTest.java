import calculator_junit.CalculatorJUnit;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Created by Shaddowrunner on 17.06.2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class CalcJUnitTest {

    @Mock
    CalculatorJUnit mockCalculator;

    @Test
    public void shouldReturnPlus() {
        Mockito.when(mockCalculator.getRandomNumberOne()).thenReturn(10);
        Mockito.when(mockCalculator.getRandomNumberTwo()).thenReturn(57);
        Mockito.when(mockCalculator.plus()).thenCallRealMethod();
        Assert.assertEquals(67, mockCalculator.plus());
    }

    @Test
    public void shouldReturnMinus() {
        Mockito.when(mockCalculator.getRandomNumberOne()).thenReturn(23);
        Mockito.when(mockCalculator.getRandomNumberTwo()).thenReturn(12);
        Mockito.when(mockCalculator.minus()).thenCallRealMethod();
        Assert.assertEquals(11, mockCalculator.minus());
    }

    @Test
    public void shouldReturnMultiply() {
        Mockito.when(mockCalculator.getRandomNumberOne()).thenReturn(10);
        Mockito.when(mockCalculator.getRandomNumberTwo()).thenReturn(12);
        Mockito.when(mockCalculator.multiply()).thenCallRealMethod();
        Assert.assertEquals(120, mockCalculator.multiply());
    }

    @Test
    public void shouldReturnDegree() {
        Mockito.when(mockCalculator.getRandomNumberOne()).thenReturn(24);
        Mockito.when(mockCalculator.getRandomNumberTwo()).thenReturn(12);
        Mockito.when(mockCalculator.degree()).thenCallRealMethod();
        Assert.assertEquals(2, mockCalculator.degree());
    }

}
